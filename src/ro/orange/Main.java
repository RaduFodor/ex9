package ro.orange;


import java.util.*;

public class Main extends HashSet{

    public static void main(String[] args) {
        //variable definition
        String[] employeesSet1 = {"Samuel Smith","Bill Williams", "Anthony Johnson","Cartner Caragher"};
        String[] list = {"Astrid Conner", "Christopher Adams", "Antoine Griezmann", "Adam Sandler", "Bailey Aidan",
                "Carl Edwin", "Carl Edwin"};
        String nume;

        // method definition
        Set MySet1 = new HashSet();      // create set of type HashSet
        for (String item : employeesSet1) {
            MySet1.add(item);
        }
        System.out.println("MySet1: " + MySet1);

        List MyList = new ArrayList();    // create List of type ArrayList
        for (String item : list){
            MyList.add(item);
        }
        System.out.println("MyList: " + MyList);

        Set MySet2 = new HashSet(MyList);   // create set with constructor MyList
        System.out.println("MySet2: " + MySet2);    //? why the change in position of elements?

        System.out.println("MySet1 matches MySet2: " + MySet1.equals(MySet2));    // compare sets using equals method

        MySet2.remove("Adam Sandler");          // remove element from MySet2
        System.out.println(MySet2);
        System.out.println("MySet1 matches MySet2: " + MySet1.equals(MySet2));

        System.out.println("MySet1 contains all elements of the list: " + MySet1.containsAll(MyList));
        System.out.println("MySet2 contains all elements of the list: " + MySet2.containsAll(MyList));

        Iterator<String> ttttt = MySet1.iterator();
        while (ttttt.hasNext()) {
            System.out.println("Iterator loop: " + ttttt.next());
        }
        MySet1.clear();
        System.out.println("MySet1 is Empty:" + ttttt.hasNext());   // check if MySet1 has elements
        System.out.println("MySet1 has " + MySet1.size() + " elements.");
        System.out.println("MySet2 has " + MySet2.size() + " elements.");

        Object[] lista = MySet2.toArray(new String[MySet2.size()]);
        System.out.println("The array: " + Arrays.toString(lista));



    }
}

